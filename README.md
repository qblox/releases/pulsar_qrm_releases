# Pulsar QRM releases

Pulsar QRM releases

Copyright (C) Qblox BV (2020) - All rights reserved

## Prerequisites:
- [Python 3.8](https://www.python.org/downloads/release/python-380/)
- [Qblox Instruments](https://pypi.org/project/qblox-instruments/)

## Install Qblox Instruments

- Open a terminal.
- Execute: `pip install qblox-instruments`

For more information, visit the [installation page](https://qblox-qblox-instruments.readthedocs-hosted.com/en/master/getting_started/installation.html)
on the Qblox Instruments Read The Docs pages.

## Update the firmware

- Download and extract the [latest release](https://gitlab.com/qblox/releases/pulsar_qrm_releases/-/archive/master/pulsar_qrm_releases-master.zip).
- In a terminal, go to the location of the extracted files.
- Execute: `qblox-cfg <ip-address> update update.tar.gz`<sup>1</sup>
- Follow the instructions on the screen.<sup>2</sup>

For more information, visit the [updating page](https://qblox-qblox-instruments.readthedocs-hosted.com/en/master/getting_started/updating.html)
on the Qblox Instruments Read The Docs pages.

---
**NOTES**

1: The update instructions have changed as of v0.7.0. When updating an instrument to any version older than v0.7.0, follow the instructions included in that releases README.

2: If the device has not rebooted after a minute, please remove power from the device for a minute before powering it on again. This forces the device to do a hard reboot. This has to be repeated twice with a one minute interval to complete the update. The update is complete when the status LED turns green again.

---
